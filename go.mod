module gitlab.com/tmaczukin-test-projects/test-gitlab-generic-package

go 1.18

require (
	github.com/magefile/mage v1.14.0
	golang.org/x/crypto v0.5.0
)
