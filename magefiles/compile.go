//go:build mage

package main

import (
	"fmt"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"github.com/magefile/mage/sh"
)

const (
	buildsDir = "builds"
)

// Compile builds the binary
func Compile() error {
	env := map[string]string{
		"CGO_ENABLED": "0",
	}

	target := filepath.Join(buildsDir, fmt.Sprintf("hello-%s-%s", getGOOS(), getGOARCH()))

	args := []string{
		"build",
		"-o", target,
		"-ldflags", strings.Join(
			[]string{
				fmt.Sprintf("-X main.VERSION=%s", getVersion()),
				fmt.Sprintf("-X main.REVISION=%s", getRevision()),
				fmt.Sprintf("-X main.REF=%s", getReference()),
				fmt.Sprintf("-X main.BUILT=%s", time.Now().Format(time.RFC3339)),
				"-w",
			},
			" ",
		),
		".",
	}

	fmt.Printf("Compiling %s... ", target)

	err := sh.RunWith(env, "go", args...)
	if err != nil {
		return fmt.Errorf("executing build command: %w", err)
	}

	fmt.Printf("DONE\n")

	return nil
}

func getGOOS() string {
	return getEnv("GOOS", runtime.GOOS)
}

func getGOARCH() string {
	return getEnv("GOARCH", runtime.GOARCH)
}
