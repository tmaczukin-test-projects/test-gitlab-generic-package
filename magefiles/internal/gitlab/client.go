package gitlab

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

const (
	gitLabAPIv4URL = "https://gitlab.com/api/v4"
)

type Client struct {
	apiV4URL string
	token    *token
}

type token struct {
	kind  string
	value string
}

func NewClient() *Client {
	return &Client{
		apiV4URL: gitLabAPIv4URL,
		token:    retrieveToken(),
	}
}

func retrieveToken() *token {
	jobToken := os.Getenv("CI_JOB_TOKEN")
	if jobToken != "" {
		fmt.Println("Using CI_JOB_TOKEN for API authentication")

		return &token{
			kind:  "JOB-TOKEN",
			value: jobToken,
		}
	}

	privateToken := os.Getenv("GITLAB_PRIVATE_TOKEN")
	if privateToken != "" {
		fmt.Println("Using GITLAB_PRIVATE_TOKEN for API authentication")

		return &token{
			kind:  "PRIVATE-TOKEN",
			value: privateToken,
		}
	}

	return nil
}

func (c *Client) do(req *http.Request, expectedCode int) (*http.Response, error) {
	c.setAPIToken(req)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("executing HTTP request: %w", err)
	}

	if resp.StatusCode != expectedCode {
		e, err2 := io.ReadAll(resp.Body)
		if err2 == nil {
			fmt.Printf("\n\nserver error: %s\n", e)
		}

		return resp, fmt.Errorf("unexpected status code: %d (expected %d)", resp.StatusCode, expectedCode)
	}

	return resp, nil
}

func (c *Client) setAPIToken(req *http.Request) {
	if c.token == nil {
		return
	}

	req.Header.Set(c.token.kind, c.token.value)
}

func (c *Client) flushBody(resp *http.Response) {
	_, _ = io.Copy(io.Discard, resp.Body)
}
