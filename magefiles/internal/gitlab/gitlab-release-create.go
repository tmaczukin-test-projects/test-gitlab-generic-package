package gitlab

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)

type GLRelease struct {
	Name    string         `json:"name"`
	TagName string         `json:"tag_name"`
	Assets  GLReleaseAsset `json:"assets"`
}

type GLReleaseAsset struct {
	Links []GLReleaseAssetLink `json:"links"`
}

type GLReleaseAssetLink struct {
	Name     string `json:"name"`
	URL      string `json:"url"`
	Filepath string `json:"filepath"`
}

func (c *Client) GitLabReleaseCreate(ctx context.Context, projectID int64, release GLRelease) error {
	u := fmt.Sprintf("%s/projects/%d/releases", c.apiV4URL, projectID)

	body := new(bytes.Buffer)

	encoder := json.NewEncoder(body)
	err := encoder.Encode(release)
	if err != nil {
		return fmt.Errorf("encoding GitLab Release: %w", err)
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, u, body)
	if err != nil {
		return fmt.Errorf("creting HTTP request: %w", err)
	}
	req.Header.Set("Content-Type", "application/json")

	resp, err := c.do(req, http.StatusCreated)
	if err != nil {
		return fmt.Errorf("executing HTTP request: %w", err)
	}

	c.flushBody(resp)

	return nil
}
