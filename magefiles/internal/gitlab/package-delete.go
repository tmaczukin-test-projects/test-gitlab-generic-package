package gitlab

import (
	"context"
	"fmt"
	"net/http"
)

func (c *Client) PackageDelete(
	ctx context.Context,
	projectID int64,
	packageID int64,
) error {
	u := fmt.Sprintf("%s/projects/%d/packages/%d", c.apiV4URL, projectID, packageID)

	req, err := http.NewRequestWithContext(ctx, http.MethodDelete, u, nil)
	if err != nil {
		return fmt.Errorf("creting HTTP request: %w", err)
	}

	resp, err := c.do(req, http.StatusNoContent)
	if err != nil {
		return fmt.Errorf("executing HTTP request: %w", err)
	}

	defer c.flushBody(resp)

	return nil
}
