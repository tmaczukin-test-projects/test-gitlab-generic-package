package gitlab

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type PackageFileUploadResponse struct {
	ID        int64 `json:"id"`
	PackageID int64 `json:"package_id"`
}

func (c *Client) PackageFileUpload(
	ctx context.Context,
	projectID int64,
	packageName string,
	packageVersion string,
	fileName string,
	file io.Reader,
) (PackageFileUploadResponse, error) {
	var r PackageFileUploadResponse

	u := fmt.Sprintf(
		"%s/projects/%d/packages/generic/%s/%s/%s?select=package_file",
		c.apiV4URL, projectID, packageName, packageVersion, fileName,
	)

	req, err := http.NewRequestWithContext(ctx, http.MethodPut, u, file)
	if err != nil {
		return r, fmt.Errorf("creating HTTP request: %w", err)
	}

	resp, err := c.do(req, http.StatusOK)
	if err != nil {
		return r, fmt.Errorf("executing HTTP request: %w", err)
	}

	defer c.flushBody(resp)

	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&r)
	if err != nil {
		return r, fmt.Errorf("decoding HTTP response: %w", err)
	}

	return r, nil
}
