package releaser

import (
	"context"
	"fmt"

	"gitlab.com/tmaczukin-test-projects/test-gitlab-generic-package/magefiles/internal/checksums"
	"gitlab.com/tmaczukin-test-projects/test-gitlab-generic-package/magefiles/internal/gitlab"
	"gitlab.com/tmaczukin-test-projects/test-gitlab-generic-package/magefiles/internal/gpg"
	"gitlab.com/tmaczukin-test-projects/test-gitlab-generic-package/magefiles/internal/packages"
	"gitlab.com/tmaczukin-test-projects/test-gitlab-generic-package/magefiles/internal/release"
)

const (
	checksumsFileName = "release.sha256"
)

type Releaser struct {
	projectID  int64
	projectURL string

	workdir string

	packageName string
	version     string

	checksumsFilePath string

	gitlab *gitlab.Client

	uploadResult packages.UploadResult
}

func New(projectID int64, projectURL string, workdir string, packageName string, version string) *Releaser {
	return &Releaser{
		projectID:   projectID,
		projectURL:  projectURL,
		workdir:     workdir,
		packageName: packageName,
		version:     version,
		gitlab:      gitlab.NewClient(),
	}
}

func (r *Releaser) GenerateChecksumsFile() error {
	cg := checksums.NewGenerator(checksumsFileName, r.workdir)

	filePath, err := cg.Generate()
	if err != nil {
		return fmt.Errorf("generating checksums file: %w", err)
	}

	r.checksumsFilePath = filePath

	return nil
}

func (r *Releaser) SignChecksumsFile(key string, password string) error {
	s, err := gpg.NewSigner(key, password)
	if err != nil {
		return fmt.Errorf("creating GPG signer: %w", err)
	}

	err = s.Sign(r.checksumsFilePath)
	if err != nil {
		return fmt.Errorf("signing checksums file: %w", err)
	}

	return nil
}

func (r *Releaser) UploadReleasePackageFiles(ctx context.Context) (int64, error) {
	u := packages.NewUploader(r.gitlab, r.projectID, r.packageName, r.version, r.workdir)
	result, err := u.Upload(ctx)
	if err != nil {
		return -1, fmt.Errorf("uploading release package files: %w", err)
	}

	r.uploadResult = result

	return result.PackageID, nil
}

func (r *Releaser) DeleteReleasePackage(ctx context.Context, packageID int64) error {
	d := packages.NewDeleter(r.gitlab, r.projectID, packageID, r.version)
	err := d.Delete(ctx)
	if err != nil {
		return fmt.Errorf("deleting release package files: %w", err)
	}

	return nil
}

func (r *Releaser) CreateGitLabRelease(ctx context.Context, tag string) error {
	c := release.NewCreator(r.gitlab, r.projectID, r.projectURL, r.version, tag)
	err := c.Create(ctx, r.uploadResult.Assets)
	if err != nil {
		return fmt.Errorf("creating GitLab Release entry: %w", err)
	}

	return nil
}

func (r *Releaser) DeleteGitLabRelease(ctx context.Context, tag string) error {
	d := release.NewDeleter(r.gitlab, r.projectID, tag)
	err := d.Delete(ctx)
	if err != nil {
		return fmt.Errorf("deleting GitLab Release entry: %w", err)
	}

	return nil
}
