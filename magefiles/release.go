package main

import (
	"context"
	"fmt"
	"os"
	"strconv"

	"gitlab.com/tmaczukin-test-projects/test-gitlab-generic-package/magefiles/internal/releaser"
)

const (
	defaultProjectID   = 42338102
	defaultProjectURL  = "https://gitlab.com/tmaczukin-test-projects/test-gitlab-generic-package"
	defaultWorkdir     = "builds"
	defaultPackageName = "release-package"
	releaseEnvFile     = "release.env"

	releasePackageIDEnv = "RELEASE_PACKAGE_ID"
	releaseTagEnv       = "RELEASE_TAG"

	ciProjectIDEnv         = "CI_PROJECT_ID"
	projectURLEnv          = "CI_PROJECT_URL"
	workdirEnv             = "WORKDIR"
	packageNameEnv         = "PACKAGE_NAME"
	gpgKeyEnv              = "GPG_KEY"
	gpgKeyFileEnv          = "GPG_KEY_FILE"
	gpgPasswordEnv         = "GPG_PASSWORD"
	gpgPasswordFileEnv     = "GPG_PASSWORD_FILE"
	ciCommitTagEnv         = "CI_COMMIT_TAG"
	createGitLabReleaseEnv = "CREATE_GITLAB_RELEASE"
)

// CreateRelease handles release creation, which includes generating and optionally
// signing checksums file, uploading release files as generic packages and optionally
// creating a GitLab Release entry
func CreateRelease(ctx context.Context) error {
	projectID, err := getProjectID()
	if err != nil {
		return err
	}

	r := releaser.New(projectID, getProjectURL(), getWorkdir(), getPackageName(), getVersion())

	err = r.GenerateChecksumsFile()
	if err != nil {
		return err
	}

	gpgKey, gpgPass, err := getGPGKeyPass()
	if err != nil {
		return err
	}

	if gpgKey != "" {
		err = r.SignChecksumsFile(gpgKey, gpgPass)
		if err != nil {
			return err
		}
	}

	packageID, err := r.UploadReleasePackageFiles(ctx)
	if err != nil {
		return err
	}

	releaseEnvF, err := os.Create(releaseEnvFile)
	if err != nil {
		return fmt.Errorf("creating %q: %w", releaseEnvFile, err)
	}
	defer releaseEnvF.Close()

	fmt.Printf("Uploaded package with ID: %d\n", packageID)
	_, err = fmt.Fprintf(releaseEnvF, "%s=%d\n", releasePackageIDEnv, packageID)
	if err != nil {
		return fmt.Errorf("writing %s to %q: %w", releasePackageIDEnv, releaseEnvFile, err)
	}

	if shouldCreateGitLabRelease() {
		err = r.CreateGitLabRelease(ctx, getTag())
		if err != nil {
			return err
		}

		_, err = fmt.Fprintf(releaseEnvF, "%s=%s\n", releaseTagEnv, getTag())
		if err != nil {
			return fmt.Errorf("writing %s to %q: %w", releaseTagEnv, releaseEnvFile, err)
		}
	}

	_ = releaseEnvF.Close()

	return nil
}

// DeleteRelease handles release deletion, which covers removing the uploaded generic package
func DeleteRelease(ctx context.Context) error {
	projectID, err := getProjectID()
	if err != nil {
		return err
	}

	packageID, err := getReleasePackageID()
	if err != nil {
		return err
	}

	r := releaser.New(projectID, getProjectURL(), getWorkdir(), getPackageName(), getVersion())
	err = r.DeleteReleasePackage(ctx, packageID)
	if err != nil {
		return err
	}

	tag := getReleaseTag()
	if shouldCreateGitLabRelease() && tag != "" {
		err = r.DeleteGitLabRelease(ctx, tag)
		if err != nil {
			return err
		}
	}

	return nil
}

func getProjectID() (int64, error) {
	return getInt64Env(ciProjectIDEnv, defaultProjectID)
}

func getInt64Env(env string, defaultVal int64) (int64, error) {
	v := os.Getenv(env)
	if v == "" {
		return defaultVal, nil
	}

	val, err := strconv.ParseInt(v, 10, 64)
	if err != nil {
		return -1, fmt.Errorf("parsing %s %q: %w", env, v, err)
	}

	return val, nil
}

func getProjectURL() string {
	return getEnv(projectURLEnv, defaultProjectURL)
}

func getEnv(env string, defaultVal string) string {
	v := os.Getenv(env)
	if v == "" {
		return defaultVal
	}

	return v
}

func getWorkdir() string {
	return getEnv(workdirEnv, defaultWorkdir)
}

func getPackageName() string {
	return getEnv(packageNameEnv, defaultPackageName)
}

func getGPGKeyPass() (string, string, error) {
	key := os.Getenv(gpgKeyEnv)
	if key == "" {
		keyFile := os.Getenv(gpgKeyFileEnv)
		if keyFile == "" {
			return "", "", nil
		}

		keyBytes, err := os.ReadFile(keyFile)
		if err != nil {
			return "", "", fmt.Errorf("reading GPG key file %q: %w", keyFile, err)
		}

		key = string(keyBytes)
	}

	pass := os.Getenv(gpgPasswordEnv)
	if pass == "" {
		passFile := os.Getenv(gpgPasswordFileEnv)
		if passFile != "" {
			passBytes, err := os.ReadFile(passFile)
			if err != nil {
				return "", "", fmt.Errorf("reading GPG password file %q: %w", passFile, err)
			}

			pass = string(passBytes)
		}
	}

	return key, pass, nil
}

func getTag() string {
	return os.Getenv(ciCommitTagEnv)
}

func shouldCreateGitLabRelease() bool {
	f := os.Getenv(createGitLabReleaseEnv)
	if f == "" {
		return false
	}

	flag, err := strconv.ParseBool(f)
	if err != nil {
		fmt.Printf("ERROR (ignored): parsing %s: %v; will skip creating GitLab Release", createGitLabReleaseEnv, err)

		return false
	}

	return flag
}

func getReleasePackageID() (int64, error) {
	return getInt64Env(releasePackageIDEnv, -1)
}

func getReleaseTag() string {
	return getEnv(releaseTagEnv, "")
}
