//go:build mage

package main

import (
	"fmt"
	"os"
	"regexp"
	"sort"
	"strings"

	"github.com/magefile/mage/sh"
)

var (
	stableVersionPattern = regexp.MustCompile("^v[0-9]+\\.[0-9]+\\.[0-9]+$")
	rcVersionPattern     = regexp.MustCompile("^v[0-9]+\\.[0-9]+\\.[0-9]+-rc[0-9]+$")

	vr = new(versionRequester)
)

type versionRequester struct {
	version   string
	revision  string
	reference string
}

func (vr *versionRequester) getVersion() string {
	if vr.version != "" {
		return vr.version
	}

	v, err := os.ReadFile("VERSION")
	if err != nil {
		v = []byte("dev")
	}

	version := strings.TrimSpace(strings.TrimPrefix(string(v), "v"))

	var exactTag string
	exactTag, err = sh.Output("git", "describe", "--exact-match")
	if err != nil {
		exactTag = ""
	}

	if stableVersionPattern.MatchString(exactTag) {
		return exactTag
	}

	if rcVersionPattern.MatchString(exactTag) {
		return exactTag
	}

	mrIID := os.Getenv("CI_MERGE_REQUEST_IID")
	if mrIID != "" {
		vr.version = fmt.Sprintf("%s-dev.mr-%s", version, mrIID)

		return vr.version
	}

	var lastTag string
	lastTag, err = sh.Output("git", "describe", "--abbrev=0", "--exclude='*-rc*'", "--always")
	if err != nil {
		lastTag = ""
	}

	var commits string
	commits, err = sh.Output("git", "rev-list", "--count", fmt.Sprintf("%s..HEAD", lastTag))
	if err != nil {
		commits = ""
	}

	vr.version = fmt.Sprintf("%s-beta.%s.g%s", version, commits, vr.getRevision())

	return vr.version
}

func (vr *versionRequester) getRevision() string {
	if vr.revision != "" {
		return vr.revision
	}

	out, err := sh.Output("git", "rev-parse", "--short=8", "HEAD")

	vr.revision = out
	if err != nil {
		vr.revision = "unknown"
	}

	return vr.revision
}

func (vr *versionRequester) getReference() string {
	if vr.reference != "" {
		return vr.reference
	}

	out, err := sh.Output("git", "show-ref")
	if err != nil {
		vr.reference = "-"

		return vr.reference
	}

	var references []string

	revision := vr.getRevision()
	for _, line := range strings.Split(out, "\n") {
		if !strings.Contains(line, revision) {
			continue
		}

		if strings.Contains(line, "HEAD") {
			continue
		}

		parts := strings.Split(line, " ")
		ref := strings.ReplaceAll(strings.ReplaceAll(parts[1], "refs/remotes/origin/", ""), "refs/heads/", "")

		references = append(references, ref)
	}

	if len(references) < 1 {
		vr.reference = "-"

		return vr.reference
	}

	sort.Strings(references)

	vr.reference = references[0]

	return vr.reference
}

func getVersion() string {
	return vr.getVersion()
}

func getRevision() string {
	return vr.getRevision()
}

func getReference() string {
	return vr.getReference()
}

// Version prints computed version information
func Version() {
	fmt.Printf("Version:   %s\n", getVersion())
	fmt.Printf("Revision:  %s\n", getRevision())
	fmt.Printf("Reference: %s\n", getReference())
}
